from aiohttp.http_exceptions import HttpProcessingError
from aiohttp import ClientSession
import asyncio
from dataclasses import dataclass
import json


class AccessTokenRequiredError(HttpProcessingError):
    """AccessTokenRequiredError

    Arguments:
        HttpProcessingError {[type]} -- [description]
    """
    pass


class ApiException(HttpProcessingError):
    """Exceptions for requests error handling

    Arguments:
        HttpProcessingError {[type]} -- [description]
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


@dataclass(init=False, eq=False, order=False, unsafe_hash=False, frozen=False)
class BitlyResponse:
    """Bilty Response class

    Returns:
        [type] -- [description]
    """

    # well, for this patricular case that method will do
    @staticmethod
    def flatten_dict(dictionary, result={}) -> dict:
        """Make flatten dict from response dict

        Arguments:
            dictionary {[type]} -- [description]

        Keyword Arguments:
            result {dict} -- [description] (default: {{}})

        Returns:
            dict -- [description]
        """
        for key, value in dictionary.items():
            if isinstance(value, dict):
                result.update(BitlyResponse.flatten_dict(value, result))
            else:
                result[key] = value
        return result

    @classmethod
    def from_response(cls, response: dict):
        """Creating new BitlyResponse

        Arguments:
            response {dict} -- [description]

        Returns:
            BitlyResponse -- [description]
        """
        resp_object = cls()
        flat_response_dict = BitlyResponse.flatten_dict(
            json.loads(response.decode()))
        for key, value in flat_response_dict.items():
            setattr(resp_object, key, value)
        return resp_object

    def __setattr__(self, name, value):
        """Magic method for adding new fields to BiltyResponse instanse

        Arguments:
            name {[type]} -- [description]
            value {[type]} -- [description]
        """
        self.__dict__[name] = value


class BitlyAPIRequest:
    """Class for wrapping request procedure

    Raises:
        ApiException: [description]

    Returns:
        [type] -- [description]
    """

    def __init__(self, session, name, params):
        """[summary]

        Arguments:
            session {[type]} -- session common for all request inside this context manager
            name {[type]} -- link or path name
            params {[type]} -- different request params
        """
        self._session = session
        self._path = name
        self._params = params

    def __getattr__(self, name):
        """Magic method for defining the api path

        Arguments:
            name {[type]} -- [description]

        Returns:
            [type] -- [description]
        """
        self._path += f'/{name}'
        return self

    async def __call__(self, **params):
        """Method creates and returnes coroutine making request to api by certain path

        Raises:
            ApiException: [description]

        Returns:
            [type] -- [description]
        """
        self._params.update(params)
        async with self._session.get(f'{self._path}', params=self._params) as response:

            if response.status != 200:
                raise ApiException(code=response.status,
                                   message='Error {}'.format(response.status))

            content = BitlyResponse.from_response(await response.read())
            if content.__dict__.get('status_code') != 200:
                raise ApiException(code=content.__dict__.get(
                    'status_code'), message=content.__dict__.get('status_txt'))
            return content


class BiltyAPI:
    """Main API class

    Raises:
        AccessTokenRequiredError: [description]

    Returns:
        [type] -- [description]
    """
    # should really find some other place, but just leave it here for some time :)
    api_url = 'https://api-ssl.bitly.com/v3/'

    def __init__(self, **credentials):
        """Init

        Raises:
            AccessTokenRequiredError: raised when no access oken specified
        """
        if 'access_token' not in credentials:
            raise AccessTokenRequiredError
        self._credentials = credentials
        self._session = ClientSession()

    async def __aenter__(self):
        # async must implement await :|. Gonna do it way
        await asyncio.sleep(0)
        return self

    async def __aexit__(self, exc_type, exc, tb):
        # closing current session
        await self._session.close()

    def __getattr__(self, name):
        """Magic method for creating new request by certain path

        Arguments:
            name {[type]} -- path or object name

        Returns:
            [type] -- [description]
        """
        return BitlyAPIRequest(self._session, self.api_url+name, self._credentials)
