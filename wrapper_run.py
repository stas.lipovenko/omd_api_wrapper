from core.model import BiltyAPI, BitlyAPIRequest, BitlyResponse, ApiException
import asyncio

if __name__ == "__main__":
    # don't tell anyone :)
    access_token = '313b6399bfc928a004a5de2bd3a978227cffd9da'

    async def api_tester():
        async with BiltyAPI(access_token=access_token) as api:
            responses = await asyncio.gather(
                # just two links for example
                api.link.clicks(link='http://bit.ly/2TC8Nvu'),
                api.link.clicks(link='http://bit.ly/368cBY2')
            )
            for response in responses:
                print(response.link_clicks)

            try:
                response = await api.link_clicks(link='bad_link')
            except ApiException as err:
                print(err)

    asyncio.run(api_tester())
